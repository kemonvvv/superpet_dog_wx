import { Router } from "../../utils/common.js";
Page({

  data: {
    petAvatar: "",
    petNo: "",
    petName: ""
  },

  onLoad: function (options) {
    if (options){
      this.setData({
        petAvatar: options.petAvatar,
        petNo: options.petNo,
        petName: options.petName
      })
    }
  },
  navToPetList: function () {
    wx.switchTab({
      url: "/pages/index/index"
    });
  }
})